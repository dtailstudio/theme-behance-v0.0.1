/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {



          var pageInputs = $('form[method="POST"]').children('input');
          $(pageInputs).prop('disabled',true);
          $(pageInputs[0]).prop('disabled',false);

          //pass match check


          $('[name="pass_repeat"]').keyup(function() {
            var password1 = $('[name="pass"]').val();
            var password2 = $('[name="pass_repeat"]').val();

            if (password1 === password2) {
              console.log("valid");
              $('[name="pass"]').closest('form').find('[type="Submit"]').prop('disabled',false);
            } else {
              console.log("invalid");
              $('[name="pass"]').closest('form').find('[type="Submit"]').prop('disabled',true);

            }
          });

          $('[name="pass"]').keyup(function() {
            if($('[name="pass"]').val().length < 6){
              $('[name="pass"]').closest('form').find('[type="Submit"]').prop('disabled',true);
              console.log('under 6 char');
            } else {
              $('[name="pass"]').closest('form').find('[type="Submit"]').prop('disabled',false);
              console.log('over 6 char');

            }
          });

          //email check
          $('[name="email"]').keyup(function() {
            var booking_email = $('input[name=email]').val();
            if (/(.+)@(.+){2,}\.(.+){2,}/.test(booking_email)) {
              console.log("mail");
              $(pageInputs).blur(function(){
                if($(event.target).val().length > 0){
                  $(event.target).next().prop('disabled',false);
                }
              });
            } else {
              // invalid email
              console.log("Not mail");

            }
          });
          //phone check
          $('#acf-field_57b5b1fc5f17b').keyup(function() {
            var phoneNumVal = $('#acf-field_57b5b1fc5f17b').val();
            var phoneNumValNew = phoneNumVal.split(' ').join('');
            $(phoneNumVal).val(phoneNumValNew);
            if ($.isNumeric(phoneNumValNew)) {
              console.log("is number");
            } else {
              console.log("is not number");
            }
          });
          //url checker

          $('#acf-field_57b5b657bbc3e').change(function() {
            var url = $('#acf-field_57b5b657bbc3e').val();
            var pattern = new RegExp("^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$");
            if (!pattern.test(url)) {
              console.log("Please enter a valid URL.");
            } else {
              console.log("You have entered a valid URL.");
            }

          });

          //email checker

          $('#acf-field_57b5b657bbc3e').change(function() {
            var url = $('#acf-field_57b5b657bbc3e').val();
            var pattern = new RegExp("^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$");
            if (!pattern.test(url)) {
              console.log("Please enter a valid URL.");
            } else {
              console.log("You have entered a valid URL.");
            }

          });


        // JavaScript to be fired on all pages

        $('.acf-field-57b558964477d .acf-input').prepend("<div class='acf-input-prepend'>I’ll <span>explain how </span>...</div>");
        $('.acf-field-57b5ac9f456ee .acf-input').prepend("<div class='acf-input-prepend'>I like <span>to do stuff</span> like...</div>");

        $('.acf-input-prepend').click(function(){
            nextelement = $(this).next();
            nextelement.focus();
            nextelement = nextelement.find('.acf-is-prepended').focus();
            $(this).hide();
        });
        $('input, textarea').focus(function(){
            $('.acf-input-prepend').hide();
        });


                // multiselect
                function customMultiSelect(){
                  $('select').selectpicker({
                    size: false,
                    width: '100%',
                    maxOptions: 3,
                    showContent: false,
                    title: 'My presentation topic will be ...'
                  });
                  $('select').on('hidden.bs.select', function(e) {
                    console.log($(this).find(":selected").text());

                  });
                  $('[data-id="acf-field_57b55a03a68bc"]').click(function() {
                    $(this).closest('div').toggleClass('open');
                  });
                }

                customMultiSelect();
                // JavaScript to be fired on all pages

                //JavaScript for file upload
                var counter = 1;

                function clickArea() {
                  var toRemove = 'C:\\fakepath\\';
                  var hiddenInputsWrapper = $('.input-hidden');
                  var hiddenInputs = $('.input-hidden').find('input[id*="acf"]');
                  console.log(hiddenInputs);
                  $('#clickArea').on('click', function() {


                    switch (counter) {
                      case 1:
                        $(hiddenInputs[0]).trigger('click');
                        $(hiddenInputsWrapper[0]).on('change', function() {
                          $(hiddenInputsWrapper[0]).addClass(' active');
                          var string = $(hiddenInputs[0]).val();
                          var finalStr = '';
                          finalStr = string.replace(toRemove, '');
                          $(hiddenInputsWrapper[0]).find('.hide-if-value').append(finalStr);

                          console.log('html',$(hiddenInputsWrapper[0]).find('.hide-if-value').html());
                          console.log('current vaule',$(hiddenInputs[0]).val());
                          console.log('input',$(hiddenInputs[0]));

                          if (counter < 3) {
                            counter++;
                          }
                        });
                        break;
                      case 2:
                        $(hiddenInputs[1]).trigger('click');
                        $(hiddenInputsWrapper[1]).on('change', function() {
                          $(hiddenInputsWrapper[1]).addClass(' active');
                          var string = $(hiddenInputs[1]).val();
                          var finalStr = '';
                          finalStr = string.replace(toRemove, '');
                          $(hiddenInputsWrapper[1]).find('.hide-if-value').append(finalStr);

                          console.log('html',$(hiddenInputsWrapper[1]).find('.hide-if-value').html());
                          console.log('current vaule',$(hiddenInputs[1]).val());
                          console.log('input',$(hiddenInputs[1]));
                          if (counter < 3) {
                            counter++;
                          }
                        });

                        break;
                      case 3:
                        $(hiddenInputs[2]).trigger('click');
                        $(hiddenInputsWrapper[2]).on('change', function() {
                          $(hiddenInputsWrapper[2]).addClass(' active');
                          var string = $(hiddenInputs[2]).val();
                          var finalStr = '';
                          finalStr = string.replace(toRemove, '');
                          $(hiddenInputsWrapper[2]).find('.hide-if-value').append(finalStr);

                          console.log('html',$(hiddenInputsWrapper[2]).find('.hide-if-value').html());
                          console.log('current vaule',$(hiddenInputs[2]).val());
                          console.log('input',$(hiddenInputs[2]));

                          if (counter < 3) {
                            counter++;
                          }
                        });

                        break;
                    }
                    if (counter === 3) {
                      $('#clickArea').css('pointer-events','none');
                    }


                  });
                }
                clickArea();
                $('.acf-labels').on('click', function(event) {
                  console.log("counter on remove",counter);
                  counter--;
                  $('#clickArea').css('pointer-events','');
                  console.log('counter',counter);
                  $('.acf-label').find('label').prop('for', '');
                  console.log('event target',event.target);
                  $(event.target).closest('.input-hidden').find('.hide-if-value').contents().filter(function() {
                    return this.nodeType === 3;
                  }).remove();
                  $(event.target).closest('.input-hidden').removeClass('active');
                });
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    },
    'press_kit': {
      init: function() {

          function readURL(input, acf_id) {
              if (input.files && input.files[0]) {
                  var reader = new FileReader();

                  reader.onload = function (e) {
                      $(acf_id).find('img').attr('src', e.target.result);
                      $(acf_id).find('.show-if-value').show();
                  };

                  reader.readAsDataURL(input.files[0]);
              }
          }

          $("#acf-field_57c95db1cd0cd, #acf-field_57c95dcecd0ce").change(function(){

              var val = $(this).val();
              var str = $(this).attr('id');

              acf_id = str.replace("_", "-");
              acf_id = '.'+acf_id;

              switch(val.substring(val.lastIndexOf('.') + 1).toLowerCase()){
                  case 'gif': case 'jpg': case 'png':
                      readURL(this, acf_id);
                      break;
                  default:
                      // error message here

                      break;
              }
          }); // END  $(input['file']).change()

      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
