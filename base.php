<?php
acf_form_head();

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

$the_ID = get_the_ID();

$current_user = wp_get_current_user();
$user_id = $current_user->ID;

if( !is_user_logged_in() && !is_super_admin() ):

    /* Check if User exist */
    if( !empty($_POST['email']) && email_exists($_POST['email']) ) {

        /* Login user with wp_signon(...) & redirect */
        $creds = array(
            'user_login'    => $_POST['email'],
            'user_password' => $_POST['pass'],
            'remember'      => true
        );

        if( wp_signon( $creds, false ) ) {
            wp_safe_redirect('/');
            exit;
        }

    }
    else if( !empty($_POST['email']) && !empty($_POST['pass']) ) {

        /* Register user - plugin dependency git@gitlab.com:dtailstudio/register-user.git */
        do_action( 'behance_register_user' );
    }

endif;



if( is_page($GLOBALS['thank_you_page_ID']) ) {
      do_action( 'send_presenter_request_email', $user_id );
}



/*
 * Approove_presenter
 */
if( is_super_admin() && is_single() && !empty($_GET["preview"]) && !empty($_GET["q"]) ) {
    do_action( 'approove_presenter', $the_ID );
}
?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>
    <div class="wrap container" role="document">
      <div class="content row">
        <main class="main">
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->
        <?php if (Setup\display_sidebar()) : ?>
          <aside class="sidebar">
            <?php include Wrapper\sidebar_path(); ?>
          </aside><!-- /.sidebar -->
        <?php endif; ?>
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>
