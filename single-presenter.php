
<?php if( is_super_admin() && get_post_status( get_the_ID() ) === 'pending' ): ?>

    <a href="<?php echo get_permalink( $post->ID ) ?>&preview=true&q=1">Approve the presenter</a>

<?php else: ?>

<?php endif; ?>
