<?php
/**
 * Template Name: Dashboard
 */
?>

<?php //get_template_part('templates/header-profile'); ?>

<?php $progress = return_progress($user_id); ?>

  <div class="row">
    <div class="col-md-6 col-md-offset-3 box dashboard-info">
      <h1 class="text-center"><?php the_title(); ?></h1>
      <div class="info-box">
        <p>We would love to know more about your presentation and how we can help you improve it. Get in detail for what you will be talking about. What’s the main topic? ....</p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-md-offset-3 box">
      <span class="progress-bar progress_<?php echo $progress['profile']; ?>"></span>
      <div class="option-tile">
        <h2 class="myProfile"><a href="/profile">My Profile</a></h2>
        <p>Here you will find all the info that you filled in the application. You can have a second look at it and edit everytink you want.</p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-md-offset-3 box">
      <span class="progress-bar progress_<?php echo $progress['press_kit']; ?>"></span>
      <div class="option-tile">
        <h2 class="pressKit"><a href="/press-kit">Press Kit</a></h2>
        <p>Here you will find all the info that you filled in the application. You can have a second look at it and edit everytink you want.</p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-md-offset-3 box">
      <span class="progress-bar progress_<?php echo 10; ?>"></span>
      <div class="option-tile">
        <h2 class="resources"><a href="/resources">Resources</a></h2>
        <p>Here you will find all the info that you filled in the application. You can have a second look at it and edit everytink you want.</p>
      </div>
    </div>
  </div>
