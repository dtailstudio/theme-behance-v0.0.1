<?php
/**
 * Template Name: FAQ
 */
?>

<h1><?php the_title(); ?></h1>

<?php
if( have_rows('faqs') ):

 	// loop through the rows of data
    while ( have_rows('faqs') ) : the_row();

        // display a sub field value
        the_sub_field('question');
        the_sub_field('answer');

    endwhile;

else :

    // no rows found

endif;
