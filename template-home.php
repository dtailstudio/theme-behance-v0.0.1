<?php
/**
 * Template Name: Home
 */
?>
<!-- 
<h1><?php the_title(); ?></h1>
<p>

<a href="http://behance.dtailstudio.dev/dashboard/">your dashboard</a>

<a href="/login">Login</a>
</p>
<?php

    // $presenters = get_field('presenters');
    $presenters = get_post_meta(get_the_ID(), 'presenters', true);
    var_dump($presenters);
    foreach ($presenters as $key => $value) {
        // var_dump($value->ID);
    }
    ?> -->




    <div class="section section-1">
    	<div class="container">
    		<h1>Behance</h1>
    		<h1>Portfolio Review 5</h1>
    		<h3>23 November 2016, Sofia, Bulgaria. </h3>
    		<div class="video col=md-12">
    			<video src="">

    			</video>
    			<div class="info">
    				<div>
    					<img src="<?php bloginfo('template_directory'); ?>/assets/images/play.png">
    					<p>Last Event Showreel</p>
    				</div>
    				<span>1MIN 20SEC</span>
    			</div>
    		</div>
    	</div>
    </div>

    <div class="section section-2">
    	<div class="container">
    		<h1>Presenters</h1>
    		<div id="myCarousel" class="carousel slide" data-ride="carousel">

    			<!-- Wrapper for slides -->
    			<div class="carousel-inner" role="listbox">
    				<div class="item active">
    					<a href="#popup1"  class="presenter col-md-3 col-sm-6">
    						<div>
    							<div class="image">
    								<img src="<?php bloginfo('template_directory'); ?>/assets/images/presenter1.jpg">
    							</div>
    							<div class="presenter-info">
    								<h3>John Doe</h3>
    								<h4>Designer</h4>
    							</div>
    						</div>
    					</a>
    					<a href="#popup1"  class="presenter col-md-3 col-sm-6">
    						<div class="image">
    							<img src="<?php bloginfo('template_directory'); ?>/assets/images/presenter1.jpg">
    						</div>
    						<div class="presenter-info">
    							<h3>John Doe</h3>
    							<h4>Designer</h4>
    						</div>
    					</a>
    					<a href="#popup1"  class="presenter col-md-3 col-sm-6">
    						<div class="image">
    							<img src="<?php bloginfo('template_directory'); ?>/assets/images/presenter3.jpg">
    						</div>
    						<div class="presenter-info">
    							<h3>John Doe</h3>
    							<h4>Designer</h4>
    						</div>
    					</a>
    					<a href="#popup1" class="presenter col-md-3 col-sm-6">
    						<div class="image">
    							<img src="<?php bloginfo('template_directory'); ?>/assets/images/presenter4.jpg">
    						</div>
    						<div class="presenter-info">
    							<h3>John Doe</h3>
    							<h4>Designer</h4>
    						</div>
    					</a>
    				</div>
    				<div class="item">
    					<a href="#popup1"  class="presenter col-md-3 col-sm-6">
    						<div class="image">
    							<img src="<?php bloginfo('template_directory'); ?>/assets/images/presenter1.jpg">
    						</div>
    						<div class="presenter-info">
    							<h3>John Doe</h3>
    							<h4>Designer</h4>
    						</div>
    					</a>
    					<a href="#popup1"  class="presenter col-md-3 col-sm-6">
    						<div class="image">
    							<img src="<?php bloginfo('template_directory'); ?>/assets/images/presenter1.jpg">
    						</div>
    						<div class="presenter-info">
    							<h3>John Doe</h3>
    							<h4>Designer</h4>
    						</div>
    					</a>
    					<a href="#popup1"  class="presenter col-md-3 col-sm-6">
    						<div class="image">
    							<img src="<?php bloginfo('template_directory'); ?>/assets/images/presenter3.jpg">
    						</div>
    						<div class="presenter-info">
    							<h3>John Doe</h3>
    							<h4>Designer</h4>
    						</div>
    					</a>
    					<a href="#popup1"  class="presenter col-md-3 col-sm-6">
    						<div class="image">
    							<img src="<?php bloginfo('template_directory'); ?>/assets/images/presenter4.jpg">
    						</div>
    						<div class="presenter-info">
    							<h3>John Doe</h3>
    							<h4>Designer</h4>
    						</div>
    					</a>
    				</div>

    				<div id="popup1" class="overlay">
    					<div class="popup container">
    						<a class="close" href="#">
    							<img src="<?php bloginfo('template_directory'); ?>/assets/images/close.png">
    						</a>
    						<div class="arrows">
    							<a class="left" href="">
    								<img src="<?php bloginfo('template_directory'); ?>/assets/images/left.png">
    							</a>
    							<a class="right" href="">
    								<img src="<?php bloginfo('template_directory'); ?>/assets/images/right.png">
    							</a>
    						</div>
    						<div class="content col-md-12">
    							<div class="picture">
    								<img src="<?php bloginfo('template_directory'); ?>/assets/images/presenter1.jpg">
    							</div>
    							<div class="p-info">
    								<h2>John Doe</h2>
    								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. </p>
    								<div class="links">
    									<a href="#">
    										<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 40 40"><title>Untitled-1</title><g id="JHBwY6.tif"><image id="Layer_1-2" data-name="Layer 1" width="40" height="40" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAACXBIWXMAAAsSAAALEgHS3X78AAAC10lEQVRYR82ZzW7UMBDHd7srpHYvtEJQqe0itj2tymYpQhUHnqHtA4AKFw5ciioegC9BP955mP9mnE5Nsh47cbWHn5Q44/HfdjzjOD0i6iUyYraZCTNlCuZIKKRsIjYjg79aggYej5gd5qUI2GM2mXVmyPSFoZRtis1U6uyIj1A70QLR4JiZM7vSeKiOz7rUnYuvoaGOSSBGYUblSJicBhiKr5n4ThaIqUJPD6nFO7SEDfE9lraiBK4xB8LA0FgqA9XOWp1NXSX0Zp/KFdjYsw7pS1v7de3VVcCQH9QZZ6QvbY79Z74hXlq8FzmntYmBtL2ly7UBVhdW1obBWS7QNoJ8FS30QwzvnsFJbqChmmpXiOiOANpFnGsLNEDLIuO4QqQg6+hhGt4zv5hbA7D7QHGxFBkHmiqByJPW9PWRwqLAH+ZC+C31Qr4d0AJNC4Ho2dRQyXFFYXHgUtX5IvVCvjXQNMIFtkPW6e1RWFiTwFuDbw00beMCUTyYtBUhYQ5M62fhJ8ULhKYJLjCU1vevSeAP5g3zlHlLzQso5FsDTVNc3AuMBvT0nch94dkcS/mJ3KdMMTQVuMAWPSbvaoHncv/Ys3ki5eeULhCajtoKPJX7Q8/mtZSfUgcC20wxguk1853KPI5E/4rKRXEjz1MFVlOcskh0CHlHpRi9GG6k3NmkCKwWSUqY+eaVYaTOqMwWZ3Q3co6vFC+wCjOxgRoZ4a/UC9mCZ2J/bbDVIB8vAnVsqnO5GI1iqi+WcCl2sP9k8K2pUh2I2SxgN4PdiTUnX1H8bubeZgHgndk1VHwo/ttuYXMYG25ysQgv5G1YwUpv+YFTvrIfTQCZAGlrJT87HRje2q/8jLjTjOf+s2XGL+hhREYffYCVPjxy6OO3HAun1fGbBi9trgPMrZrn0QKdU/QUIaDtEXBBHR8Ba9whOnofe4g+o4yH6HX4vyFwnuJ+Q8ypo98Q/wAd8Rrzbh7EbAAAAABJRU5ErkJggg=="/></g></svg>
    									</a>
    									<a href="#">
    										<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 40 40"><title>Untitled-1</title><g id="BnZA5i.tif"><image id="Layer_1-2" data-name="Layer 1" width="40" height="40" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAACXBIWXMAAAsSAAALEgHS3X78AAACyUlEQVRYR82Zz2tTQRDHkyYKbaTY9lJIm9q0p6hJqBcVvZSCeCniwYO3HgstIiJYFNv/fDrfvNnHZrsvO/OSLe/wgcf+mPm+/TGzb1+LiFo16TG7zJAZMRPmRJhI2VDa9BT2oiQbBDxl+sxrEbDPbDHrTJdpC10p25I2I+nTFxspP2aBcDhgpsyeOE/1CVmXvlOx1VX0UQnEKIypGAmV0QRdsTUW27UFYqrwpq9oiTW0gA2xPRBfJoFrzLHQUTirS8fzsxZrE+uEtzmiYgdWvtkKaYuvo5i/WAcM+XGscUba4nMQ1oUNsWixLnJOaxUd8b3tl/sNsLuwszYUxnIB3wjyZbTwKzG8+wojuYGGcqpdIaI7Augq4tyyQAO0zDKOK0QKasLoOZBxoKkUiDxZJ33lAlqgaSYQWWKk6OR3XrTLXzLXzC1zyRwyO1ScalK2faCp15KOlun9wHyuqHtBhbA7j7/MH2ZTYdsHmnbxgCieTNoeb8TxOfMkqPtG8+IcN8w7hW0faBriAUNpWX/PqBgVOP4ljjGFyAYXFBd4rrAbAk0jPMwFRgXY/qf0UMQ/5n+kHLxX2A2BpgkecES35F3sLiz+W4qLidFX2A2BphM8WAViim8oLcrx22jfUQq0TjHAxrqmtDjwSWEvRjnF1k0CEJquKC0O6/K5wl6McpNYw4zjLRXxrUoc+KiwU0UZZqyBOjRStR6/U8UxXgny8SxQW1MdXgjp7IyKDRAT94Xs6zqkTHXAclhwB9uvzA8qwg34KWUHChsp5g4LAHFqT9HxsXhw3EJ2qBNucjALLxQcWEGjj/zAKW/sRxPAJ19jPzsdGN7oV35G3G3GQVi3qPEhPY5I89UHaPTlkcO/fsuxcZa6fvPBos11gbkdqTcLdEbxpggBy14BT2jFV8A+7hIdb2+9RB9Txkv0GOFvCNynuN8QU1rRb4h7BUEA1UYTyzYAAAAASUVORK5CYII="/></g></svg>
    									</a>
    									<a href="#">
    										<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 40 40"><title>Untitled-1</title><g id="O91QvS.tif"><image id="Layer_1-2" data-name="Layer 1" width="40" height="40" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAACXBIWXMAAAsSAAALEgHS3X78AAACfklEQVRYR82ZTUsDMRCGrS2C7aleLNRWbHsq2hb/gSheFA+CBz2JgiCIghfxoPSfx3nrZDtds5vZTVZ6eGDJziRvvibZ2Q1jzEZJWkSHGBBjYkocM1MuG7BNS1GfE69Bii2iSxyxgB7RJraJBlFjGlzWZpsx+3S5Dl87hQWiwT4xI/a4cZ9Pmm32nXFdDYWPSiBGYWJ+R0JVqYcG1zXhuksLxFShp4cmYA3l0OS6+9xWIYGbxIipKxorS120s+mycTmhN0PzuwMzexaRGrc1dLXncsCQj1zGFVLjNvvpd2lDLFqsiyqnNYs6t70jy6UBdhd2VlNRWVWgbQT5JFrIlxjenqKSqoGGZKptIaI7AmiMOBcKNEDL4sSxhTiCQkcPC/2U+CTmAp+fC5w40JQIxDlZ5viSoNdzBz4/F9ACTQuBOCXGCicft2Yp6oM4J84UfllAUwsPuA6FTi94MEuBlwp7H9DUwQOiuPfQViAFXijsfUDTAA8YytD190R8m6XAL+KFOFH4ZgFNYzysBMaSzDO4U/hmAU1TPOCKHnruvhHvZinsk8uuFL5ZQNNxLIFg18Rdg4nAGFMMpMBrhb2PZIpjbBIgBd4o7H0kmyRWmJECQzaHJQkzsQK1FHivsPeB83gRqGMddVLgo8LeR3LUgRiXBSnwWWGfx8plAeBqs6dwzEMKfFXY5/HnuoXLYaxwE8oivJjUhRWs9ZUfWOVr+9EE8Mm3tp+dFgyv8yu/Qmw2Yz/9Ls/4wPyPyMKpD7DWySOLTL9VsXGC0m8SLNqqEpg7jveFBdpK0VOEgNAU8NRETgFLbBIdvS+aRJ+YCpPoLtK/IZBZsL8hZibSb4gfD7TmgcSEdhwAAAAASUVORK5CYII="/></g></svg>
    									</a>
    								</div>
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>

    			<!-- Left and right controls -->
    			<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    				<span class="sr-only">Previous</span>
    			</a>
    			<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    				<span class="sr-only">Next</span>
    			</a>
    		</div>

    	</div>
    </div>

    <div class="section section-3">
    	<div class="container">
    		<h1>Whats Happening</h1>
    		<h2>bro. its f*!$cking raining outside</h2>
    		<div class="grid">
    			<div class="coll coll-1 col-md-3 col-sm-6">
    				<a href="#" class="item small">
    					<div class="tip-content">
    						<span>Article</span>
    						<h4>Say hello to Nikola Uzunov </h4>
    					</div>
    				</a>
    				<a href="#" class="item large">
    					<div class="tip-content">
    						<span>Article</span>
    						<h4>Cultured Pearls As A Gift</h4>
    					</div>
    				</a>
    			</div>
    			<div class="coll coll-2 col-md-3 col-sm-6">
    				<a href="#" class="item large">
    					<div class="tip-content">
    						<span>Article</span>
    						<h4>The Glossary Of Telescopes</h4>
    					</div>
    				</a>
    				<a href="#" class="item small">
    					<!-- <img src="<?php bloginfo('template_directory'); ?>/assets/images/col2-2.jpg"> -->
    				</a>
    			</div>
    			<div class="coll coll-3 col-md-3 col-sm-6">
    				<a href="#" class="item small">
    					<!-- <img src="<?php bloginfo('template_directory'); ?>/assets/images/col3.jpg"> -->
    				</a>
    				<a href="#" class="item large">
    					<div class="tip-content">
    						<span>Article</span>
    						<h4>When The Morning Dawns</h4>
    					</div>
    				</a>
    			</div>
    			<div class="coll coll-4 col-md-3 col-sm-6">
    				<a href="#" class="item large dtail">
    					<div class="tip-content">
    						<h4><span>We are hiring</span></h4>
    						<h4>The Glossary Of Telescopes</h4>
    					</div>
    				</a>
    				<a href="#" class="item small">
    					<!-- <img src="<?php bloginfo('template_directory'); ?>/assets/images/col4-2.jpg"> -->
    				</a>
    			</div>
    		</div>
    	</div>
    </div>

    <div class="section section-4">
    	<div class="container">
    		<h1>Schedule</h1>
    		<div class="row">
    			<div class="col-md-4 col-sm-4 left">
    				<h4>Welcome Drinks</h4>
    				<h4><span>08:00 - 10:00 AM</span></h4>
    			</div>
    			<div class="col-md-8 col-sm-8 right">
    				<p>Join leading creative minds in a more intimate creative development workshop setting. Then, discuss your gameplan over lunch with your new friends before heading to Alice Tully Hall for our first batch of main stage talks</p>
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-md-4 col-sm-4 left">
    				<h4>Conference Talks</h4>
    				<h4><span>10:00 - 11:00 AM</span></h4>
    			</div>
    			<div class="col-md-8 col-sm-8 right">
    				<p>A series of exceptionally productive creative leaders take the stage to talk about execution, sharing road-tested insights culled from their extensive experience with translating ideas from vision to reality.</p>
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-md-4 col-sm-4 left">
    				<h4>Cocktail Reception</h4>
    				<h4><span>11:00 - 12:00 AM</span></h4>
    			</div>
    			<div class="col-md-8 col-sm-8 right">
    				<p>Get to know fellow attendees over complimentary cocktails in the beautiful Alice Tully atrium.</p>
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-md-4 col-sm-4 left">
    				<h4>Conference Talks</h4>
    				<h4><span>01:00 - 02:00 PM</span></h4>
    			</div>
    			<div class="col-md-8 col-sm-8 right">
    				<p>A series of exceptionally productive creative leaders take the stage to talk about execution, sharing road-tested insights culled from their extensive experience with translating ideas from vision to reality.</p>
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-md-4 col-sm-4 left">
    				<h4>Welcome Drinks</h4>
    				<h4><span>08:00 - 10:00 AM</span></h4>
    			</div>
    			<div class="col-md-8 col-sm-8 right">
    				<p>Join leading creative minds in a more intimate creative development workshop setting. Then, discuss your gameplan over lunch with your new friends before heading to Alice Tully Hall for our first batch of main stage talks</p>
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-md-4 col-sm-4 left">
    				<h4>Conference Talks</h4>
    				<h4><span>10:00 - 11:00 AM</span></h4>
    			</div>
    			<div class="col-md-8 col-sm-8 right">
    				<p>A series of exceptionally productive creative leaders take the stage to talk about execution, sharing road-tested insights culled from their extensive experience with translating ideas from vision to reality.</p>
    			</div>
    		</div>
    	</div>
    </div>

    <div class="section section-5">
    	<div class="container">
    		<h1>Buy a Ticket</h1>
    		<table>
    			<tr class="bar row">
    				<th class="col-md-4 col-sm-4">
    					<h4>TICKET TYPE</h4>
    				</th>
    				<th class="col-md-2 col-md-offset-4 col-sm-2 col-sm-offset-4">
    					<h4>DATE</h4>
    				</th>
    				<th class="col-md-2 col-sm-2">
    					<h4>PRICE</h4>
    				</th>
    			</tr>
    			<tr class="row">
    				<td class="col-md-4 col-sm-4">
    					<h3>Super early bird ticket</h3>
    					<h4>back seats</h4>
    				</td>
    				<td class="col-md-2 col-md-offset-4 col-sm-2 col-sm-offset-4">
    					<h3>27.11.2016</h3>
    				</td>
    				<td class="col-md-2 col-sm-2">
    					<h3>$50</h3>
    				</td>
    			</tr>
    			<tr class="row">
    				<td class="col-md-4 col-sm-4">
    					<h3>Super early bird ticket</h3>
    					<h4>back seats</h4>
    				</td>
    				<td class="col-md-2 col-md-offset-4 col-sm-2 col-sm-offset-4">
    					<h3>27.11.2016</h3>
    				</td>
    				<td class="col-md-2 col-sm-2">
    					<h3>$50</h3>
    				</td>
    			</tr>
    			<tr class="row">
    				<td class="col-md-4 col-sm-4">
    					<h3>Super early bird ticket</h3>
    					<h4>back seats</h4>
    				</td>
    				<td class="col-md-2 col-md-offset-4 col-sm-2 col-sm-offset-4">
    					<h3>27.11.2016</h3>
    				</td>
    				<td class="col-md-2 col-sm-2">
    					<h3>$50</h3>
    				</td>
    			</tr>
    		</table>
    	</div>
    </div>

    <div class="section section-6">
    	<div class="container">
    		<div class="sponsors">
    			<h1>Sponsors</h1>
    			<div class="content">
    				<a href="#">
    					<img src="<?php bloginfo('template_directory'); ?>/assets/images/adobe.png">
    				</a>
    				<a href="#">
    					<img src="<?php bloginfo('template_directory'); ?>/assets/images/facebook.png">
    				</a>
    				<a href="#">
    					<img src="<?php bloginfo('template_directory'); ?>/assets/images/spotify.png">
    				</a>
    			</div>
    		</div>
    		<div class="about">
    			<h1>About</h1>
    			<div class="col-md-6 col-1">
    				<p>When television was young, there was a hugely popular show based on the still popular fictional character of Superman. The opening of that show had a familiar phrase that went, “Look. Up in the sky. It’s a bird. It’s a plane. It’s Superman!” How beloved Superman has become in our culture and the worldwide fascination with extraterrestrials and all things cosmic only emphasizes that there is a deep curiosity in all humans about nature and astronomy, even if many people would not know to call it astronomy.</p>
    				<p>Astronomy is one of the oldest sciences of all time. When archeologists unearth ancient civilizations, even as far back as the cavemen, they invariably find art that shows mans unquenchable fascination with the stars. To this day, you can easily get an animated discussion at any gathering on the topic of “Is there intelligent life on other planets?” </p>
    			</div>
    			<div class="col-md-6 col-2">
    				<p>Many have tried to explain mankind’s seeming obsession with outer space as a result of an ancient memory or as part of mankind’s eternal nature. Whatever the cause, people of every age and every nation share this one deep interest, to know more about the universe that our tiny planet is just a part of.</p>
    			</div>
    		</div>
    		<div class="footer">
    			<div class="copyright">
    				<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18.01 18.02"><defs><style>.cls-1{fill:#fff;fill-rule:evenodd;}</style></defs><title>Untitled-1</title><g id="Dtail_Logo" data-name="Dtail Logo"><path class="cls-1" d="M395,138h-4v4h4Zm7,0h-4v4h4Zm3,0v14H395v-3h7v-4H391v11h18V138Z" transform="translate(-391 -138)"/></g></svg>
    			    <p>
    			        Copyright <?php echo date('Y'); ?>. All rights reserverd.
    			    </p>
    			</div>
    			<div class="links">
    				<a href="#"><svg id="fb" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10.02 18.99"><defs><style>.cls-1{fill:#fff;fill-rule:evenodd;}</style></defs><title>Untitled-2</title><g id="Shape_2" data-name="Shape 2"><path class="cls-1" d="M401.23,141.34a2,2,0,0,1,1.36-.46,3.37,3.37,0,0,1,1.69.7l.73-2.85a11,11,0,0,0-3.2-.73,7,7,0,0,0-3,.57,3.56,3.56,0,0,0-1.56,1.64,5.86,5.86,0,0,0-.42,2.55V144H395v3h1.86v10h4V147h2.49l.69-3h-3.17v-1.13A2.14,2.14,0,0,1,401.23,141.34Z" transform="translate(-394.99 -138.01)"/></g></svg></a>
     				<a href="#"><svg id="twitter" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 27 19"><defs><style>.cls-1{fill:#fff;fill-rule:evenodd;}</style></defs><title>Untitled-3</title><g id="Shape_1" data-name="Shape 1"><path class="cls-1" d="M410.94,144.45a3.27,3.27,0,0,0,2.94-1.79,4.74,4.74,0,0,1-3.18.36c0-.23-.1-.44-.15-.63-.71-2.68-3.16-4.84-5.72-4.58l.63-.24c.28-.1,1.94-.38,1.68-1s-2.24.4-2.62.52c.5-.19,1.33-.52,1.42-1.11a4,4,0,0,0-2.11,1,1.49,1.49,0,0,0,.4-.82c-2.05,1.34-3.25,4-4.23,6.67a10.16,10.16,0,0,0-2-1.68,53.07,53.07,0,0,0-6.93-3.11,3.75,3.75,0,0,0,2.31,3.47,5.56,5.56,0,0,0-1.66.2c.23,1.24,1,2.26,3,2.75a2.45,2.45,0,0,0-1.85.75,3.12,3.12,0,0,0,3.32,1.66c-2.07.91-.84,2.6.84,2.35a7.05,7.05,0,0,1-10,.27c6.8,9.46,21.58,5.6,23.78-3.52a4,4,0,0,0,3.22-1.24A6.45,6.45,0,0,1,410.94,144.45Z" transform="translate(-387 -136)"/></g></svg></a>
     				<a href="#"><svg id="be" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 27.01 16.01"><defs><style>.cls-1{fill:#fff;fill-rule:evenodd;}</style></defs><title>Untitled-4</title><g id="Shape_4" data-name="Shape 4"><path class="cls-1" d="M411.83,139h-7v2.36h7Zm-14.14,7.18a3.44,3.44,0,0,0,2.22-3.27c0-2.92-2.11-3.89-5.22-3.89H387V154.7h7.92c3,0,5.77-1.36,5.77-4.55A3.85,3.85,0,0,0,397.68,146.18Zm-7.1-4.48H394c1.28,0,2.47.31,2.47,1.8,0,1.32-.94,1.87-2.2,1.87h-3.64ZM394.43,152h-3.85v-4.31h3.91c1.53,0,2.59.64,2.59,2.24S395.85,152,394.43,152Zm13.81-9a5.81,5.81,0,0,0-6,6,5.66,5.66,0,0,0,6,6,5.31,5.31,0,0,0,5.54-3.82h-2.86a2.69,2.69,0,0,1-2.56,1.47c-1.81,0-2.82-.9-2.91-2.88H414C414.22,146.29,412.27,143,408.24,143Zm-2.79,4.77a2.51,2.51,0,0,1,2.7-2.42c1.58,0,2.29.84,2.59,2.42Z" transform="translate(-386.99 -139)"/></g></svg></a>
    				<a href="#"><svg id="ball" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19 19"><defs><style>.cls-1{fill:#fff;fill-rule:evenodd;}</style></defs><title>Untitled-5</title><g id="Shape_3" data-name="Shape 3"><path class="cls-1" d="M400.5,138a9.5,9.5,0,1,0,9.5,9.5A9.51,9.51,0,0,0,400.5,138Zm6.28,4.38a8.07,8.07,0,0,1,1.83,5.05,19,19,0,0,0-5.65-.26l-.17-.42c-.17-.4-.35-.79-.54-1.17A10.81,10.81,0,0,0,406.78,142.38Zm-6.28-3a8.08,8.08,0,0,1,5.38,2,9.15,9.15,0,0,1-4.25,2.93,43.4,43.4,0,0,0-3-4.75A8.11,8.11,0,0,1,400.5,139.4Zm-3.45.77a51.64,51.64,0,0,1,3,4.69,30.26,30.26,0,0,1-7.5,1A8.14,8.14,0,0,1,397,140.17Zm-4.66,7.34c0-.08,0-.17,0-.25a29.92,29.92,0,0,0,8.34-1.15c.23.45.45.92.66,1.38l-.32.1a12.88,12.88,0,0,0-6.6,5.35A8.08,8.08,0,0,1,392.38,147.51Zm8.11,8.11a8.07,8.07,0,0,1-5-1.71,11.13,11.13,0,0,1,6.37-5.07l.05,0a33.73,33.73,0,0,1,1.73,6.15A8.07,8.07,0,0,1,400.5,155.63Zm4.53-1.39a35,35,0,0,0-1.58-5.79,11.92,11.92,0,0,1,5.05.35A8.13,8.13,0,0,1,405,154.24Z" transform="translate(-391 -138)"/></g></svg></a>
    			</div>
    		</div>
    	</div>
    </div>
