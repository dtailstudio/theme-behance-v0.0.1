<?php
/**
 * Template Name: Login presenter Template
 */
?>

<h1><?php the_title(); ?></h1>

<a href="#">
	<svg id="Layer_1" class="logo-dtail" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 59 59.01"><defs><style>.cls-1{fill:#fff;fill-rule:evenodd;}</style></defs><title>Untitled-1</title><g id="Dtail_Logo" data-name="Dtail Logo"><path class="cls-1" d="M385.06,118H371v14.06h14.06ZM408,118H394v14h14Zm8,0-.07,14.11v30.77H385.12L385,155h23V141H371v36h59V118Z" transform="translate(-371.01 -118)"/></g></svg>
</a>

<form action="" method="POST">
	<div class="log-input email">
		<input name=email type="text">
		<p>My <span>email address</span> is...</p>
	</div>
	<div class="log-input pass">
		<input name=pass type="password">
		<p>My <span>password</span> is...</p>
	</div>
	<div class="btns">
		<a href="<?php echo get_permalink( get_page_by_path( 'present' ) ); ?>" class="btn btn-default btn-login btn-login-back">Back</a>
		<input type="submit" class="btn btn-default btn-login" value=login>
	</div>
</form>
<!-- <fb:login-button scope="public_profile,email" onlogin="checkLoginState();"></fb:login-button> -->

<a href="/register" class="register">Not have account? Register!</a>
