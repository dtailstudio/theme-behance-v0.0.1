<?php
/**
 * Template Name: Past events
 */
?>

<h1><?php the_title(); ?></h1>



<?php

$args = array (
            'post__in'               => array( '640', '639', '632' ),
            'post_type'              => array( 'events' ),
            'post_status'            => array( 'publish' ),
        );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post();
?>

<div class="col-md-4 event event-1">
  <a href="<?php echo get_permalink() ?>">
    <img src="<?php the_field('cover_image') ?>">
    <div class="info">
      <h3><?php the_title() ?></h3>
      <p><?php the_field('attendees') ?> attendees</p>
    </div>
  </a>
</div>

<?php
endwhile;
