<?php
/**
 * Template Name: Present
 */
?>

<!-- <h1><?php the_title(); ?></h1> -->

<?php
if( !is_user_logged_in() ) {
    ?>
    <div class="section section-1">
    	<div class="container">
    		<h1><?php the_title(); ?></h1>
    		<h3>Present to the biggest graphic design and art audience in Bulgaria. </h3>
    		<div class="btns col-md-5 col-md-offset-3">
    			<a href="/signup" href="#" class="btn btn-default btn-register">sign up</a>
    			<a href="/login" class="btn btn-default btn-register btn-register-login">login</a>
    		</div>
    	</div>
    </div>
    <?php
}
?>



<div class="section section-2">
	<div class="container">
		<h1>What you get</h1>
		<div class="links">
			<a href="/profile" class="link link-profile">
				<div class="tip-content">
					<h3>Profile</h3>
					<p>Here you will find all the info that you filled in the application. </p>
				</div>
			</a>
			<a href="press-kit" class="link link-press-kit">
				<div class="tip-content">
					<h3>Press Kit</h3>
					<p>Here you will find all the info that you filled in the application. </p>
				</div>
			</a>
			<a href="/resources" class="link link-resources">
				<div class="tip-content">
					<h3>Resources</h3>
					<p>Here you will find all the info that you filled in the application. </p>
				</div>
			</a>
		</div>
	</div>
</div>

<div class="section section-3">
	<div class="container">
		<h1>Some Facts</h1>
		<div class="facts">
			<div class="col-md-4 fact">
				<h1>500+</h1>
				<span>attendees</span>
			</div>
			<div class="col-md-4 fact">
				<h1>50+</h1>
				<span>presenters</span>
			</div>
			<div class="col-md-4 fact">
				<h1>230+</h1>
				<span>liters of beer drinked</span>
			</div>
		</div>
	</div>
</div>

<div class="section section-4">
	<div class="container">
		<h1>Presenter Reviews</h1>
		<div id="myCarousel" class="carousel slide" data-ride="carousel">

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<div class="review col-md-6">
						<div class="text">
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis.
	</p>
						</div>
						<div class="presenter">
							<img src="<?php bloginfo('template_directory'); ?>/assets/images/presenter.png">
							<div class="presenter-info">
								<h3>Nikola Uzunov</h3>
								<h4>Presenter at BPR / 4</h4>
							</div>
						</div>
					</div>
					<div class="review col-md-6">
						<div class="text">
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis.
	</p>
						</div>
						<div class="presenter">
							<img src="<?php bloginfo('template_directory'); ?>/assets/images/presenter.png">
							<div class="presenter-info">
								<h3>Nikola Uzunov</h3>
								<h4>Presenter at BPR / 4</h4>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="review col-md-6">
						<div class="text">
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis.
	</p>
						</div>
						<div class="presenter">
							<img src="<?php bloginfo('template_directory'); ?>/assets/images/presenter.png">
							<div class="presenter-info">
								<h3>Nikola Uzunov</h3>
								<h4>Presenter at BPR / 4</h4>
							</div>
						</div>
					</div>
					<div class="review col-md-6">
						<div class="text">
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis.
	</p>
						</div>
						<div class="presenter">
							<img src="<?php bloginfo('template_directory'); ?>/assets/images/presenter.png">
							<div class="presenter-info">
								<h3>Nikola Uzunov</h3>
								<h4>Presenter at BPR / 4</h4>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Left and right controls -->
			<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>

	</div>
</div>

<div class="section section-5">
	<div class="container">
		<h1>Past Events</h1>
		<div class="events">
			<div class="col-md-4 event">
				<a href="#" class="review-4">
					<div class="info">
						<h3>Behance</h3>
						<h3>Portfolio Review 4</h3>
						<h4>135 attendees</h4>
					</div>
				</a>
			</div>
			<div class="col-md-4 event">
				<a href="#" class="review-3">
					<div class="info">
						<h3>Behance</h3>
						<h3>Portfolio Review 3</h3>
						<h4>135 attendees</h4>
					</div>
				</a>
			</div>
			<div class="col-md-4 event">
				<a href="#" class=" review-2">
					<div class="info">
						<h3>Behance</h3>
						<h3>Portfolio Review 2</h3>
						<h4>135 attendees</h4>
					</div>
				</a>
			</div>
		</div>
	</div>
</div>

<div class="section section-6">
	<div class="container">
		<div class="sponsors">
			<h1>Sponsors</h1>
			<div class="content">
				<a href="#">
					<img src="<?php bloginfo('template_directory'); ?>/assets/images/adobe.png">
				</a>
				<a href="#">
					<img src="<?php bloginfo('template_directory'); ?>/assets/images/facebook.png">
				</a>
				<a href="#">
					<img src="<?php bloginfo('template_directory'); ?>/assets/images/spotify.png">
				</a>
			</div>
		</div>
		<div class="about">
			<h1>About</h1>
			<div class="col-md-6 col-1">
				<p>When television was young, there was a hugely popular show based on the still popular fictional character of Superman. The opening of that show had a familiar phrase that went, “Look. Up in the sky. It’s a bird. It’s a plane. It’s Superman!” How beloved Superman has become in our culture and the worldwide fascination with extraterrestrials and all things cosmic only emphasizes that there is a deep curiosity in all humans about nature and astronomy, even if many people would not know to call it astronomy.</p>
				<p>Astronomy is one of the oldest sciences of all time. When archeologists unearth ancient civilizations, even as far back as the cavemen, they invariably find art that shows mans unquenchable fascination with the stars. To this day, you can easily get an animated discussion at any gathering on the topic of “Is there intelligent life on other planets?” </p>
			</div>
			<div class="col-md-6 col-2">
				<p>Many have tried to explain mankind’s seeming obsession with outer space as a result of an ancient memory or as part of mankind’s eternal nature. Whatever the cause, people of every age and every nation share this one deep interest, to know more about the universe that our tiny planet is just a part of.
</p>
			</div>
		</div>
		<div class="footer">
			<div class="copyright">
				<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18.01 18.02"><defs><style>.cls-1{fill:#fff;fill-rule:evenodd;}</style></defs><title>Untitled-1</title><g id="Dtail_Logo" data-name="Dtail Logo"><path class="cls-1" d="M395,138h-4v4h4Zm7,0h-4v4h4Zm3,0v14H395v-3h7v-4H391v11h18V138Z" transform="translate(-391 -138)"/></g></svg>
			    <p>
			        Copyright <?php echo date('Y'); ?>. All rights reserverd.
			    </p>
			</div>
			<div class="links">
				<a href="#"><svg id="fb" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10.02 18.99"><defs><style>.cls-1{fill:#fff;fill-rule:evenodd;}</style></defs><title>Untitled-2</title><g id="Shape_2" data-name="Shape 2"><path class="cls-1" d="M401.23,141.34a2,2,0,0,1,1.36-.46,3.37,3.37,0,0,1,1.69.7l.73-2.85a11,11,0,0,0-3.2-.73,7,7,0,0,0-3,.57,3.56,3.56,0,0,0-1.56,1.64,5.86,5.86,0,0,0-.42,2.55V144H395v3h1.86v10h4V147h2.49l.69-3h-3.17v-1.13A2.14,2.14,0,0,1,401.23,141.34Z" transform="translate(-394.99 -138.01)"/></g></svg></a>
 				<a href="#"><svg id="twitter" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 27 19"><defs><style>.cls-1{fill:#fff;fill-rule:evenodd;}</style></defs><title>Untitled-3</title><g id="Shape_1" data-name="Shape 1"><path class="cls-1" d="M410.94,144.45a3.27,3.27,0,0,0,2.94-1.79,4.74,4.74,0,0,1-3.18.36c0-.23-.1-.44-.15-.63-.71-2.68-3.16-4.84-5.72-4.58l.63-.24c.28-.1,1.94-.38,1.68-1s-2.24.4-2.62.52c.5-.19,1.33-.52,1.42-1.11a4,4,0,0,0-2.11,1,1.49,1.49,0,0,0,.4-.82c-2.05,1.34-3.25,4-4.23,6.67a10.16,10.16,0,0,0-2-1.68,53.07,53.07,0,0,0-6.93-3.11,3.75,3.75,0,0,0,2.31,3.47,5.56,5.56,0,0,0-1.66.2c.23,1.24,1,2.26,3,2.75a2.45,2.45,0,0,0-1.85.75,3.12,3.12,0,0,0,3.32,1.66c-2.07.91-.84,2.6.84,2.35a7.05,7.05,0,0,1-10,.27c6.8,9.46,21.58,5.6,23.78-3.52a4,4,0,0,0,3.22-1.24A6.45,6.45,0,0,1,410.94,144.45Z" transform="translate(-387 -136)"/></g></svg></a>
 				<a href="#"><svg id="be" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 27.01 16.01"><defs><style>.cls-1{fill:#fff;fill-rule:evenodd;}</style></defs><title>Untitled-4</title><g id="Shape_4" data-name="Shape 4"><path class="cls-1" d="M411.83,139h-7v2.36h7Zm-14.14,7.18a3.44,3.44,0,0,0,2.22-3.27c0-2.92-2.11-3.89-5.22-3.89H387V154.7h7.92c3,0,5.77-1.36,5.77-4.55A3.85,3.85,0,0,0,397.68,146.18Zm-7.1-4.48H394c1.28,0,2.47.31,2.47,1.8,0,1.32-.94,1.87-2.2,1.87h-3.64ZM394.43,152h-3.85v-4.31h3.91c1.53,0,2.59.64,2.59,2.24S395.85,152,394.43,152Zm13.81-9a5.81,5.81,0,0,0-6,6,5.66,5.66,0,0,0,6,6,5.31,5.31,0,0,0,5.54-3.82h-2.86a2.69,2.69,0,0,1-2.56,1.47c-1.81,0-2.82-.9-2.91-2.88H414C414.22,146.29,412.27,143,408.24,143Zm-2.79,4.77a2.51,2.51,0,0,1,2.7-2.42c1.58,0,2.29.84,2.59,2.42Z" transform="translate(-386.99 -139)"/></g></svg></a>
				<a href="#"><svg id="ball" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19 19"><defs><style>.cls-1{fill:#fff;fill-rule:evenodd;}</style></defs><title>Untitled-5</title><g id="Shape_3" data-name="Shape 3"><path class="cls-1" d="M400.5,138a9.5,9.5,0,1,0,9.5,9.5A9.51,9.51,0,0,0,400.5,138Zm6.28,4.38a8.07,8.07,0,0,1,1.83,5.05,19,19,0,0,0-5.65-.26l-.17-.42c-.17-.4-.35-.79-.54-1.17A10.81,10.81,0,0,0,406.78,142.38Zm-6.28-3a8.08,8.08,0,0,1,5.38,2,9.15,9.15,0,0,1-4.25,2.93,43.4,43.4,0,0,0-3-4.75A8.11,8.11,0,0,1,400.5,139.4Zm-3.45.77a51.64,51.64,0,0,1,3,4.69,30.26,30.26,0,0,1-7.5,1A8.14,8.14,0,0,1,397,140.17Zm-4.66,7.34c0-.08,0-.17,0-.25a29.92,29.92,0,0,0,8.34-1.15c.23.45.45.92.66,1.38l-.32.1a12.88,12.88,0,0,0-6.6,5.35A8.08,8.08,0,0,1,392.38,147.51Zm8.11,8.11a8.07,8.07,0,0,1-5-1.71,11.13,11.13,0,0,1,6.37-5.07l.05,0a33.73,33.73,0,0,1,1.73,6.15A8.07,8.07,0,0,1,400.5,155.63Zm4.53-1.39a35,35,0,0,0-1.58-5.79,11.92,11.92,0,0,1,5.05.35A8.13,8.13,0,0,1,405,154.24Z" transform="translate(-391 -138)"/></g></svg></a>
			</div>
		</div>
	</div>
</div>
