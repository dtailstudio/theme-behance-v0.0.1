<?php
/**
 * Template Name: Press kit Project
 */
?>

<h1 class="text-left"><?php the_title(); ?></h1>

<div class="showcase">
    <?php
    switch (get_the_ID()) {
        case $GLOBALS['press_kit_project1_page_ID']:
            $form_group = $GLOBALS['press_kit_field_group1_ID'];
            break;
        case $GLOBALS['press_kit_project2_page_ID']:
            $form_group = $GLOBALS['press_kit_field_group2_ID'];
            break;
        case $GLOBALS['press_kit_project3_page_ID']:
            $form_group = $GLOBALS['press_kit_field_group3_ID'];
            break;
        case $GLOBALS['press_kit_project4_page_ID']:
            $form_group = $GLOBALS['press_kit_field_group4_ID'];
            break;

        default:
            # code...
            break;
    }

    acf_form(array(
    	'field_groups' 					=> array($form_group),
    	'post_id'						=> $GLOBALS['user_id'],
    	'instruction_placement' 		=> 'field',
    	// 'return'						=> home_url('/profile/?q=good'),
    	'submit_value'					=> 'Save'
    ));
    ?>
</div>
