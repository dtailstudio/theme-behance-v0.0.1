<?php
/**
 * Template Name: Press kit
 */
?>

<h1 class="text-left"><?php the_title(); ?></h1>

  <div class="row">
    <div class="col-md-4">
      <?php
	  if(!empty(get_field('profile_image', $GLOBALS['user_id']))) {
          $submit_value = 'Change';
	  } else {
          $submit_value = 'Upload';
	  }

    	acf_form(array(
    		'field_groups' 				=> array($GLOBALS['press_kit_page_ID']),
    		'post_id'					=> $GLOBALS['user_id'],
    		'fields' 					=> array('field_57c95db1cd0cd'),
    		'submit_value'				=> $submit_value
    	));
        ?>
      <div class="">
          <h2>Profile Image</h2>
          <p>We’ll use that image for our presenters page. Reccomended size: 500x500px.</p>
      </div>
    </div>
    <div class="col-md-4">
      <?php
    	acf_form(array(
    		'field_groups' 			=> array($GLOBALS['press_kit_page_ID']),
    		'post_id' 				=> $GLOBALS['user_id'],
    		'fields' 				=> array('field_57c95dcecd0ce'),
    		'submit_value'			=> $submit_value
    	));
     ?>
      <div class="">
          <h2>Cover Image</h2>
          <p>We’ll use that image for our presenters page. Reccomended size: 500x500px.</p>
      </div>
    </div>
    <div class="col-md-4">
      <img src="smiley.gif" alt="Smiley face" height="42" width="42">
      <h2>Showcase Projects</h2>
      <p>We’ll use those to  to drive more interest to your
        presentation. Recommended size: 500x500px.</p>
      <a class="btn btn-default btn-default-normal">Create</a>
    </div>
  </div>
  <div class="projects">
      <?php
      $num_of_projects = '0';
      $href = array();

      $file_url1 = get_field('project_file_1_1', $GLOBALS['user_id']);
      if($file_url1) {
          $href[0] = '/press-kit-project-1';
          $num_of_projects += 1;
      } else { $href[0] = '#'; }

      $file_url2 = get_field('project_file_1_2', $GLOBALS['user_id']);
      if($file_url2) {
          $href[1] = '/press-kit-project-2';
          $num_of_projects += 1;
      } else { $href[1] = '#'; }

      $file_url3 = get_field('project_file_1_3', $GLOBALS['user_id']);
      if($file_url3) {
          $href[2] = '/press-kit-project-3';
          $num_of_projects += 1;
      } else { $href[2] = '#'; }

      $file_url4 = get_field('project_file_1_4', $GLOBALS['user_id']);
      if($file_url4) {
          $href[3] = '/press-kit-project-4';
          $num_of_projects += 1;
      } else { $href[3] = '#'; }

      ?>
    <h2>Showcased Projects</h2>
    <h2 class="count"><span><?php echo $num_of_projects; ?></span>/4</h2>
    <div class="row">
        <?php
        ?>
        <a href="<?php echo $href[0]; ?>">
        <div class="col-md-3">
            <img src="<?php echo $file_url1['url'] ?>" alt="" >
        </div>
        </a>
        <?php
        ?>
        <a href="<?php echo $href[1]; ?>">
        <div class="col-md-3">
            <img src="<?php echo $file_url2['url'] ?>" alt="" >
        </div>
        </a>
        <?php
        ?>
        <a href="<?php echo $href[2]; ?>">
        <div class="col-md-3">
            <img src="<?php echo $file_url3['url'] ?>" alt="" >
        </div>
        </a>
        <?php
        ?>
        <a href="<?php echo $href[3]; ?>">
        <div class="col-md-3">
            <img src="<?php echo $file_url4['url'] ?>" alt="" >
        </div>
        </a>
    </div>
  </div>

<div class="">

    <?php

    $forms = array(
        $GLOBALS['press_kit_field_group1_ID'],
        $GLOBALS['press_kit_field_group2_ID'],
        $GLOBALS['press_kit_field_group3_ID'],
        $GLOBALS['press_kit_field_group4_ID']
    );

    foreach ($forms as $key => $value) {
        ?>
        <div class="showcase _<?php echo $key; ?>">
            <?php
            acf_form(array(
            	'field_groups' 					=> array($value),
            	'post_id'						=> $GLOBALS['user_id'],
            	'instruction_placement' 		=> 'field',
            	'submit_value'					=> 'Save'
            ));
            ?>
        </div>
        <?php
    }
    ?>

</div>
