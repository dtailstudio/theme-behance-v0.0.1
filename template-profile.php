<?php
/**
 * Template Name: Profile
 */
?>

<h1><?php the_title(); ?></h1>

<?php

acf_form(array(
	'field_groups' 					=> array(244),
	'post_id'						=> '50',
	'instruction_placement' 		=> 'field',
	'return'						=> home_url('/profile/?q=good'),
	'submit_value'					=> 'Save'
));



$topics = get_field('topics', '50');

// var_dump($topics);

foreach ($topics as $key => $value) {
    echo $value;
}

// if( get_the_ID() === $my_profile_page_edit_ID ) {
//     get_template_part('templates/profile', 'edit');
// } else {
//     get_template_part('templates/profile', '');
// }
