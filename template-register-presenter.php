<?php
/**
 * Template Name: Register presenter
 */

/* Submit form to base.php */
?>

<h1><?php the_title(); ?></h1>
<div class="box">
	<div class="info-box">
		<p>We would love to know more about your presentation and how we can help you improve it. Get in detail for what you will be talking about. What’s the main topic? ....</p>
	</div>
</div>
<form action="" method="POST">
	<div class="register-input">
		<input name=email type="email" placeholder="My email address is...">
		<p>My <span>email address</span> is...</p>
	</div>
	<div class="register-input">
		<input name=pass type="password" class=pass>
		<p class=pass>My <span>password</span> is...</p>
	</div>
	<div class="register-input">
		<input name=pass_repeat type="password">
		<p>Repeating my <span>password</span>...</p>
	</div>
	<div class="checkbox">
		<input type="checkbox" name="subscribe" id=subscribe>
		<label for="subscribe">Subscribe me for <span>future updates</span></label>
	</div>
	<div class="btns">
		<a class="btn btn-default btn-register btn-register-back">cancel</a>
		<input type="submit" value="next step" class="btn-register">
	</div>
</form>

<a href="/login" class="already-registered">already registered?</a>
