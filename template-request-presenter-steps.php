<?php
/**
 * Template Name: Request presenter All Steps
 */
?>

<h1><?php the_title(); ?></h1>
<div class="box">
	<div class="info-box">
		<p>We would love to know more about your presentation and how we can help you improve it. Get in detail for what you will be talking about. What’s the main topic? ....</p>
	</div>
</div>

<?php
switch( get_the_ID() ) {
	case $GLOBALS['step1_page_ID'];
		$fields = array('field_57b5587a4477c', 'field_57b558964477d'); $submit_value = 'Step 2'; $redirect_url = '/step2/';
		break;
	case $GLOBALS['step2_page_ID'];
		$fields = array('field_57b55a03a68bc', 'field_57b55b48a68bd'); $submit_value = 'Step 3'; $redirect_url = '/step3/';
		break;
	case $GLOBALS['step3_page_ID'];
		$fields = array('field_57b5ac58456ec', 'field_57b5ac7b456ed', 'field_57b5ac9f456ee'); $submit_value = 'Step 4'; $redirect_url = '/step4/';
		break;
	case $GLOBALS['step4_page_ID'];
		$fields = array('field_57b5b1fc5f17b', 'field_57b5b2235f17c'); $submit_value = 'Step 5'; $redirect_url = '/step5/';
		break;
	case $GLOBALS['step5_page_ID'];
		echo "<div id=clickArea></div>";
		$fields = array('field_57b5b657bbc3e', 'field_57b5b76cfe96f', 'field_57b5be88149dd', 'field_57b5be94149de'); $submit_value = 'Done'; $redirect_url = '/thank-you/';
		break;
}

$submit_value = 'Next Step';

acf_form(array(
	'post_id'		=> $user_id,
	'fields' => $fields,
	'instruction_placement' => 'field',
	'return'		=> home_url( $redirect_url ),
	'submit_value'	=> $submit_value
));

if ( get_the_ID() !== $GLOBALS['request_presenter_page_ID'] ):
	previous_post_link('%link', 'Back');
endif;
