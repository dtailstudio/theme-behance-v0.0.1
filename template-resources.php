<?php
/**
 * Template Name: Resources
 */
?>

  <h1><?php the_title(); ?></h1>
  <div class="row">
    <div class="col-md-4 box box-resources box-resources-photoshop ">
      <span class="icon"></span>
      <h2>Photoshop Template</h2>
      <p>This is a guideline so you can build your presentation proprely</p>
      <a class="btn btn-default btn-default-download" download href="<?php $ps = get_field('photoshop'); echo $ps['url']; ?>">Download</a>
    </div>
    <div class="col-md-4 box box-resources box-resources-keynote">
      <span class="icon"></span>
      <h2>Keynote Template</h2>
      <p>This is a guideline so you can build your presentation proprely</p>
      <a class="btn btn-default btn-default-download" download href="<?php the_field('keynote'); ?>">Download</a>
    </div>
    <div class="col-md-4 box box-resources box-resources-powerpoint">
      <span class="icon"></span>
      <h2>Powerpoint Template</h2>
      <p>This is a guideline so you can build your presentation proprely</p>
      <a class="btn btn-default btn-default-download" download href="<?php the_field('powerpoint'); ?>">Download</a>
    </div>
  </div>
  <h2>Tips and Tricks</h2>
  <p>Read about tips and tricks of how to do a great presentation. We collected some importnant information that we think can be helpful for your presentation</p>

  <div class="row">
      <?php if( have_rows('articles') ): ?>

          <?php while( have_rows('articles') ): the_row(); ?>

              <a class="col-md-6 article" href="<?php the_sub_field('url') ?>" target="_parent">
                <h3><?php the_sub_field('type') ?></h3>
                <p><?php the_sub_field('title') ?></p>
              </a>

          <?php endwhile; ?>

      <?php endif; ?>
  </div>
