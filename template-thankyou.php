<?php
/**
 * Template Name: Thank You
 */
?>

  <h1><?php the_title(); ?></h1>
  <div class="row">
      <h2><span>Your request is sent.</h2></h2>
      <p>You applied for presenting at Behance Portfolio Review 5.</p>
      <p>We’ll review your infromation and contact you shortly.</p>
      <a href="<?php the_field('ok_button_target') ?>" class="btn btn-default btn-extra-large">Ok</a>
  </div>
  <div class="row">

    <h2>Past Events</h2>


    <?php
    $args = array (
            	'post__in'               => array( '640', '639', '632' ),
            	'post_type'              => array( 'events' ),
            	'post_status'            => array( 'publish' ),
            );
    $loop = new WP_Query( $args );
    while ( $loop->have_posts() ) : $loop->the_post();
    ?>

    <div class="col-md-4 event event-1">
      <a href="<?php echo get_permalink() ?>">
        <img src="<?php the_field('cover_image') ?>">
        <div class="info">
          <h3><?php the_title() ?></h3>
          <p><?php the_field('attendees') ?> attendees</p>
        </div>
      </a>
    </div>

    <?php
    endwhile;
    ?>
    <div class="col-md-4 event event-1">
      <a href="#">
        <img src="">
        <div class="info">
          <h3>Behance</h3>
          <h3>Portfolio Review 4</h3>
          <p>135 attendees</p>
        </div>
      </a>
    </div>
    <div class="col-md-4 event event-2">
      <a href="#">
        <img src="">
        <div class="info">
          <h3>Behance</h3>
          <h3>Portfolio Review 3</h3>
          <p>124 attendees</p>
        </div>
      </a>
    </div>
    <div class="col-md-4 event event-3">
      <a href="#">
        <img src="">
        <div class="info">
          <h3>Behance</h3>
          <h3>Portfolio Review 2</h3>
          <p>84 attendees</p>
        </div>
      </a>
    </div>
  </div>
