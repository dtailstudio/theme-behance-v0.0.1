<footer class="content-info">
    <p>
        Copyright <?php echo date('Y'); ?>. All rights reserverd.
    </p>
    <ul class="nav-footer">
        <li>
          <a href="#" class="fb">
            <svg id="fb" data-name="fb" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 6.9 15.31"><defs><style>.cls-1{fill:#fff;fill-rule:evenodd;}</style></defs><title>Untitled-4</title><path class="cls-1" d="M401.7,142.6v1.5l2.1.3v2.4h-2.1l.3,7.8h-3v-7.8h-1.8v-2.4l1.2-.3v-1.5s-.27-2.86,2.4-3.3h3.3v2.4h-1.5A.88.88,0,0,0,401.7,142.6Z" transform="translate(-397.2 -139.29)"/></svg>
          </a>
        </li>
        <li>
            <a href="#" class="twitter">
              <svg id="twitter" data-name="twitter" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15.02 12.3"><defs><style>.cls-1{fill:#fff;fill-rule:evenodd;}</style></defs><title>twitter</title><path class="cls-1" d="M407.1,141.2l1.51-.3a12.74,12.74,0,0,1-1.51,1.5h0a4.79,4.79,0,0,0,0,.9c-2.89,12.37-13.51,6.9-13.51,6.9,3.87.1,4.21-1.5,4.21-1.5-2.26-.34-2.7-1.8-2.7-1.8a.94.94,0,0,0,1.2-.3,2.71,2.71,0,0,1-2.1-3,2.88,2.88,0,0,0,1.47.57c-.28-.32-2.56-3.05-1.17-4.47a7.48,7.48,0,0,0,6.08,3.28l.3-.07A3.15,3.15,0,0,1,404,139.1c2.1,0,2.55,1.2,2.55,1.2l1.5-.9S407.84,140.9,407.1,141.2Z" transform="translate(-393.59 -139.1)"/></svg>
            </a>
        </li>
        <li>
            <a href="#" class="instagram">
              <svg id="instagram" data-name="instagram" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 15"><defs><style>.cls-1{fill:#fff;}</style></defs><title>Untitled-8</title><path class="cls-1" d="M404.9,140.19h-9.6a2.7,2.7,0,0,0-2.7,2.7v9.6a2.7,2.7,0,0,0,2.7,2.7h9.6a2.7,2.7,0,0,0,2.7-2.7v-9.6A2.7,2.7,0,0,0,404.9,140.19Zm-4.81,5.12a2.39,2.39,0,1,1-2.41,2.39A2.4,2.4,0,0,1,400.09,145.31Zm5.71,6.29a1.62,1.62,0,0,1-1.8,1.8h-7.8a1.62,1.62,0,0,1-1.8-1.8v-5.4h1.79a4.2,4.2,0,1,0,7.83,0h1.78Zm0-7.19h-2.41V142h2.41Z" transform="translate(-392.6 -140.19)"/></svg>
            </a>
        </li>
        <li>
          <a href="#" class="in">
            <svg id="in" data-name="in" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15.01 14.41"><defs><style>.cls-1{fill:#fff;fill-rule:evenodd;}</style></defs><title>Untitled-7</title><path class="cls-1" d="M407.59,148.94v-.63h-.06c-.3-2.45-1.68-3.58-3.53-3.71a3.34,3.34,0,0,0-3,1.76V145h-3v9.6h3v-6.47a2.32,2.32,0,0,1,2.1-.88,1.52,1.52,0,0,1,1.49,1.15v6.19h3V149h0S407.59,149,407.59,148.94ZM393.2,154.6h2.4V145h-2.4Zm1.19-14.41a1.81,1.81,0,1,0,1.8,1.81A1.8,1.8,0,0,0,394.39,140.19Z" transform="translate(-392.59 -140.19)"/></svg>
          </a>
        </li>
        <li>
            <a href="#" class="pinterest">
              <svg id="pinterest" data-name="pinterest" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 11.45 15.31"><defs><style>.cls-1{fill:#fff;}</style></defs><title>Untitled-9</title><path class="cls-1" d="M401,139.89a5.57,5.57,0,0,0-6,5.71,3.18,3.18,0,0,0,2.1,3l.3-1.5c-.44-.55-1.09-1.09-.3-3.3a3.75,3.75,0,0,1,4.8-2.4c2.22.64,2.75,1.36,2.7,3.9s-2.2,4-3,3.9a1.73,1.73,0,0,1-1.2-.9l-.15-.23c.38-1.61.68-3,.68-3,.43-2.25-1.18-1.84-1.18-1.84-2,.38-1.18,3.38-1.18,3.38s-.85,4.28-.88,5.22c0,.61-.29,3.37-.29,3.37a20.64,20.64,0,0,0,1.47-2.14,25.8,25.8,0,0,0,1-3.62c.64,1.44,2.58,1,2.58,1,3.32-.74,3.57-2.9,3.9-4.5A5.36,5.36,0,0,0,401,139.89Z" transform="translate(-395 -139.89)"/></svg>
            </a>
        </li>
        <li>
            <a href="#" class="go">
              <svg id="go" data-name="go" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14.41 13.52"><defs><style>.cls-1{fill:#fff;}</style></defs><title>Untitled-10</title><path class="cls-1" d="M401.3,151.3a4.16,4.16,0,0,0-1.5-3c-.76-.55-1-.87-1.14-1.17a3.15,3.15,0,0,0,2-2.87,3,3,0,0,0-.56-1.72l1.43-1.07s.16-.68-.3-.67-3.18.27-3.76.32l-.26,0a3.31,3.31,0,0,0-3.44,3.16,3.25,3.25,0,0,0,3,3.11v1.25c-2.17.11-3.89,1.33-3.89,2.84s1.88,2.86,4.2,2.86,4.2-1.28,4.2-2.86c0-.06,0-.11,0-.16Zm-5.46-6.76c-.24-1.15.19-2.22,1-2.38s1.6.65,1.85,1.8-.19,2.22-1,2.38S396.08,145.69,395.84,144.53Zm1.41,8.56a1.84,1.84,0,1,1,2.25-1.8A2.06,2.06,0,0,1,397.25,153.09Zm9.47-6.59h-1.81v-1.81a.6.6,0,0,0-.6-.6h0a.6.6,0,0,0-.6.6v1.81h-1.78a.59.59,0,1,0,0,1.19h1.78v1.81a.6.6,0,0,0,.6.6h0a.6.6,0,0,0,.6-.6v-1.81h1.81a.59.59,0,0,0,0-1.19Z" transform="translate(-392.91 -140.79)"/></svg>
            </a>
        </li>
    </ul>
</footer>
