<nav id=profile-nav>
  <?php
  if (has_nav_menu('profile_navigation')) :
    wp_nav_menu(['theme_location' => 'profile_navigation', 'menu_class' => 'nav']);
  endif;

  if ( !is_page_template( array('template-press-kit.php')) ) :
    ?>
    <input id=submit_acf_form type="submit" value="save" class="save--button">
    <?php
  endif;
  ?>
</nav>

<?php $progress = return_progress($GLOBALS['user_id'], basename( get_page_template() )) ?>

<span class="progress  progress_<?php echo $progress; ?>"></span>

<?php
if( get_post_status( $GLOBALS['user_id'] ) === 'pending' ) {
    echo 'Pending<hr>';
} else {
    echo 'Approoved!';
}
?>

<script>
jQuery( "#submit_acf_form" ).click(function( event ) {
    jQuery( "#post" ).submit();
});
</script>
