<header class="banner">
    <?php

    $is_white = '';
    $templates = array('template-dashboard.php', 'template-profile.php', 'template-press-kit.php', 'template-press-kit-project.php', 'template-resources.php');

    if( is_page_template( $templates ) ) :
        $where = 'dashboard';
    elseif( is_page_template(array('template-request-presenter-steps.php')) ) :
        $where = 'login';
        $is_white = 'white';
    elseif (has_nav_menu('main_navigation')) :
        $where = 'login';
    endif;
    ?>

    <nav class="navbar navbar-default navbar-<?php echo $where . ' ' . $is_white; ?>" role="navigation">
        <div class="navbar-brand-<?php echo $where; ?>">
            <a class="brand" href="<?= esc_url(home_url('/')); ?>">
                <svg id="behance" data-name="behance" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26.42 16"><defs><style>.cls-1{fill:#fff;fill-rule:evenodd;}</style></defs><title>Untitled-1</title><path class="cls-1" d="M96.32,119.89a3.57,3.57,0,0,0,2.76-3.66c0-2.13-1.53-3.72-4.57-3.72h-8v16h8a4.68,4.68,0,0,0,4.89-4.71A3.84,3.84,0,0,0,96.32,119.89Zm-6.85-4.75h4.26a1.68,1.68,0,0,1,1.87,1.68,1.65,1.65,0,0,1-1.87,1.68H89.47Zm4.41,10H89.47v-3.64h4.41a1.82,1.82,0,0,1,2,1.82C95.91,124.46,95.14,125.13,93.88,125.13Zm19-2.51c0-3.76-2.32-6.35-5.8-6.35a6.07,6.07,0,1,0,.19,12.15,7.21,7.21,0,0,0,4.77-1.61l-1.37-2a4.83,4.83,0,0,1-3.07,1.13,3.1,3.1,0,0,1-3.33-2.59h8.6Zm-8.63-1.39a2.88,2.88,0,0,1,5.7,0Zm6.2-8.73h-8v2h8Z" transform="translate(-86.5 -112.5)"/></svg>
                <?php //bloginfo('name'); ?>
            </a>
            <?php if( is_page_template( $templates ) ) { ?>
                <span>
                    Welcome <?php  the_field('first_name', $GLOBALS['user_id']) ?>
                </span>
            <?php } ?>
        </div>
        <?php
        if( is_page_template( $templates ) ) :
            wp_nav_menu(['theme_location' => 'profile_main_navigation', 'menu_class' => 'nav nav-dashboard']);
        elseif (has_nav_menu('main_navigation')) :
            wp_nav_menu(['theme_location' => 'main_navigation', 'menu_class' => 'nav nav-login']);
        endif;
        ?>
    </nav>

</header>

<?php
if( is_page_template( $templates ) && !is_page_template( array('template-dashboard.php') ) ) :
    get_template_part('templates/header-profile');
endif;
if( is_page_template(array('template-request-presenter-steps.php')) ) :

    switch( get_the_ID() ) {
    	case $GLOBALS['step1_page_ID'];
    		$progress = 2;
    		break;
    	case $GLOBALS['step2_page_ID'];
    		$progress = 4;
    		break;
    	case $GLOBALS['step3_page_ID'];
    		$progress = 6;
    		break;
    	case $GLOBALS['step4_page_ID'];
    		$progress = 8;
    		break;
    	case $GLOBALS['step5_page_ID'];
    		$progress = 10;
    		break;
    }
    ?>
    <span class="progress  progress_<?php echo $progress; ?>"></span>
    <?php
endif;
?>
<!--
<div class="sidebar hidden">
<?php
if (has_nav_menu('primary_navigation')) :
wp_nav_menu(['theme_location' => 'main_navigation', 'menu_class' => 'nav']);
wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
endif;
?>
</div> -->
